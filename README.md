# Automotive SIG

This is the central repository for the Automotive SIG. It contains receipes for
building the images, information on how to download them as well as
documentation for these.

If you are looking for more information you can visit:

* The Automotive SIG wiki page: https://wiki.centos.org/SpecialInterestGroup/Automotive
* The Automotive SIG documentation: https://sigs.centos.org/automotive

