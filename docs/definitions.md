# Vocabulary and definitions used by the Automotive SIG

This page tries to define vocabulary and terms used by the Automotive SIG which
can sometime be new to the reader or subject to different interpretation.

If you feel that something is not clear, please let us know, we will gladly
improve our wording, add it to this list or both :)

## OSBuild Manifests

OSBuild Manifests are JSON files used instruct [osbuild](https://www.osbuild.org/)
on how to build images.

## SIG / Special Interest Group

A SIG or Special Interest Group is a group of person interested in the same topic
and willing to collaborate together to move this topic further.
In the case of the CentOS Automotive SIG, we are a group of person interested
in developing an Operating System (OS) based on CentOS-Stream that could be used
in an automotive environment. However, this OS will not be safety certified and
should be considered as a research project or a Proof Of Concept (POC).
